<!--

    Copyright 2014-2025 smartics, Kronseder & Reiner GmbH

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<ac:layout>
  <ac:layout-section ac:type="single">
    <ac:layout-cell>
      <ac:structured-macro ac:name="projectdoc-properties-marker">
        <ac:parameter ac:name="doctype">journal-space</ac:parameter>
        <ac:parameter ac:name="render-as">definition-list</ac:parameter>
        <ac:parameter ac:name="extract-short-desc">true</ac:parameter>
        <ac:rich-text-body>
          <div class="table-wrap">
            <table class="confluenceTable">
              <tbody>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.shortDescription"/></th>
                  <td class="confluenceTd"><at:var at:name="projectdoc.doctype.common.shortDescription.context.create-space" at:rawxhtml="true" /></td>
                  <td class="confluenceTd"></td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.name"/></th>
                  <td class="confluenceTd"><at:var at:name="name" /><at:var at:name="projectdoc_doctype_common_name" /></td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.subject"/></th>
                  <td class="confluenceTd"><ac:structured-macro ac:name="projectdoc-name-list">
                      <ac:parameter ac:name="doctype">subject</ac:parameter>
                      <ac:parameter ac:name="property"><at:i18n at:key="projectdoc.doctype.common.subject"/></ac:parameter>
                    </ac:structured-macro></td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.categories"/></th>
                  <td class="confluenceTd"><ac:structured-macro ac:name="projectdoc-name-list">
                      <ac:parameter ac:name="doctype">category</ac:parameter>
                      <ac:parameter ac:name="property"><at:i18n at:key="projectdoc.doctype.common.categories"/></ac:parameter>
                    </ac:structured-macro></td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.tags"/></th>
                  <td class="confluenceTd"><ac:structured-macro ac:name="projectdoc-tag-list-macro">
                      <ac:parameter ac:name="property"><at:i18n at:key="projectdoc.doctype.common.tags"/></ac:parameter>
                    </ac:structured-macro></td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.flags"/></th>
                  <td class="confluenceTd">projectdoc.space-home, projectdoc.all</td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.spaceTags"/></th>
                  <td class="confluenceTd"><ac:structured-macro ac:name="projectdoc-display-space-attribute-macro">
                    <ac:parameter ac:name="attribute">labels</ac:parameter></ac:structured-macro></td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th class="confluenceTh"><at:i18n at:key="projectdoc.doctype.common.sortKey"/></th>
                  <td class="confluenceTd"><ac:placeholder><at:i18n at:key="projectdoc.doctype.common.sortKey.placeholder"/></ac:placeholder></td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th class="confluenceTh">documentation-json-uri</th>
                  <td class="confluenceTd">https://www.smartics.eu/confluence/download/attachments/12156954/docmap.json?api=v2</td>
                  <td class="confluenceTd">hide</td>
                </tr>
                <tr>
                  <th class="confluenceTh">projectdoc.projectdoc-table-merger-macro.param.discard.activate.no-items</th>
                  <td class="confluenceTd">false</td>
                  <td class="confluenceTd">hide</td>
                </tr>
              </tbody>
            </table>
          </div>
        </ac:rich-text-body>
      </ac:structured-macro>
    </ac:layout-cell>
  </ac:layout-section>

  <ac:layout-section ac:type="single">
    <ac:layout-cell>
      <ac:structured-macro ac:name="panel">
        <ac:parameter ac:name="borderStyle">none</ac:parameter>
        <ac:rich-text-body>
          <p>
            <ac:structured-macro ac:name="livesearch">
              <ac:parameter ac:name="placeholder"><at:i18n at:key="projectdoc.home.label.search"/></ac:parameter>
              <ac:parameter ac:name="spaceKey"><at:var at:name="spaceKeyElement" at:rawxhtml="true"/></ac:parameter>
              <ac:parameter ac:name="size">large</ac:parameter>
            </ac:structured-macro>
          </p>
        </ac:rich-text-body>
      </ac:structured-macro>
    </ac:layout-cell>
  </ac:layout-section>
  <ac:layout-section ac:type="three_with_sidebars">
    <ac:layout-cell>
      <ac:structured-macro ac:name="projectdoc-aside-panel-macro">
        <ac:parameter ac:name="css">projectdoc-pagetype-spacehome, projectdoc-addon-agile-planning, projectdoc-featured</ac:parameter>
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.home.label.featured-pages"/></ac:parameter>
        <ac:rich-text-body>
          <p>
            <ac:structured-macro ac:name="contentbylabel" ac:schema-version="3">
              <ac:parameter ac:name="showLabels">false</ac:parameter>
              <ac:parameter ac:name="showSpace">false</ac:parameter>
              <ac:parameter ac:name="sort">title</ac:parameter>
              <ac:parameter ac:name="cql">label = "featured" and space = currentSpace() and type = "page"</ac:parameter>
            </ac:structured-macro>
          </p>
        </ac:rich-text-body>
      </ac:structured-macro>


      <ac:structured-macro ac:name="projectdoc-content-marker">
        <ac:parameter ac:name="id">documentation-core</ac:parameter>
        <ac:parameter ac:name="css">projectdoc-homepage-panel-core</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-aside-panel-macro">
            <ac:parameter ac:name="css">projectdoc-pagetype-spacehome, projectdoc-addon-agile-planning, projectdoc-panel-core, projectdoc-nav-panel</ac:parameter>
            <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.content.documentation"/></ac:parameter>
            <ac:rich-text-body>
              <ul>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.tour.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.faq.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.topic.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.glossary-item.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
              </ul>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="projectdoc-content-marker">
        <ac:parameter ac:name="id">library-core</ac:parameter>
        <ac:parameter ac:name="css">projectdoc-homepage-panel-core</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-aside-panel-macro">
            <ac:parameter ac:name="css">projectdoc-pagetype-spacehome, projectdoc-addon-agile-planning, projectdoc-panel-core, projectdoc-nav-panel</ac:parameter>
            <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.content.resources"/></ac:parameter>
            <ac:rich-text-body>
              <ul>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.resource.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.quote.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
              </ul>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="projectdoc-content-marker">
        <ac:parameter ac:name="id">content-organization</ac:parameter>
        <ac:parameter ac:name="css">projectdoc-homepage-panel-core</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-aside-panel-macro">
            <ac:parameter ac:name="css">projectdoc-pagetype-spacehome, projectdoc-addon-agile-planning, projectdoc-panel-core, projectdoc-nav-panel</ac:parameter>
            <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.content.section.content-organization"/></ac:parameter>
            <ac:rich-text-body>
              <ul>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.subject.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.category.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.tag.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.types.aggregate-home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
              </ul>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="projectdoc-content-marker">
        <ac:parameter ac:name="id">team-core</ac:parameter>
        <ac:parameter ac:name="css">projectdoc-homepage-panel-core</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-aside-panel-macro">
            <ac:parameter ac:name="css">projectdoc-pagetype-spacehome, projectdoc-addon-agile-planning, projectdoc-panel-core, projectdoc-nav-panel</ac:parameter>
            <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.content.references.section.hr"/></ac:parameter>
            <ac:rich-text-body>
              <ul>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.role.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.stakeholder.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.person.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.organization.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
              </ul>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="projectdoc-hide-from-anonymous-user-macro">
        <ac:parameter ac:name="atlassian-macro-output-type">BLOCK</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-content-marker">
            <ac:parameter ac:name="id">content-collaboration</ac:parameter>
            <ac:parameter ac:name="css">projectdoc-homepage-panel-core</ac:parameter>
            <ac:rich-text-body>
              <ac:structured-macro ac:name="projectdoc-aside-panel-macro">
                <ac:parameter ac:name="css">projectdoc-pagetype-spacehome, projectdoc-addon-agile-planning, projectdoc-panel-core, projectdoc-nav-panel</ac:parameter>
                <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.content.section.content-collaboration"/></ac:parameter>
                <ac:rich-text-body>
                  <ul>
                    <li>
                      <ac:structured-macro ac:name="projectdoc-link-wiki">
                        <ac:parameter ac:name="page">projectdoc.content.charter.home.title</ac:parameter>
                      </ac:structured-macro>
                    </li>
                    <li>
                      <ac:structured-macro ac:name="projectdoc-link-wiki">
                        <ac:parameter ac:name="page">projectdoc.content.docmodule.home.title</ac:parameter>
                      </ac:structured-macro>
                    </li>
                  </ul>
                </ac:rich-text-body>
              </ac:structured-macro>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
    </ac:layout-cell>
    <ac:layout-cell>
      <p>
        <ac:structured-macro ac:name="blog-posts">
          <ac:parameter ac:name="content">excerpts</ac:parameter>
          <ac:parameter ac:name="max">3</ac:parameter>
        </ac:structured-macro>
      </p>
    </ac:layout-cell>
    <ac:layout-cell>
      <at:var at:name="team" at:rawxhtml="true"/>
      <ac:structured-macro ac:name="projectdoc-content-marker">
        <ac:parameter ac:name="id">volume-core</ac:parameter>
        <ac:parameter ac:name="css">projectdoc-homepage-panel-core</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-aside-panel-macro">
            <ac:parameter ac:name="css">projectdoc-pagetype-spacehome, projectdoc-addon-agile-planning, projectdoc-panel-agile-planning, projectdoc-nav-panel</ac:parameter>
            <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.content.planning.volumes"/></ac:parameter>
            <ac:rich-text-body>
              <ul>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.doctype.announcement.home</ac:parameter>
                    <ac:parameter ac:name="label"><at:i18n at:key="projectdoc.volume.project-blackboard.title"/></ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.volume.teams-listing.title</ac:parameter>
                    <ac:parameter ac:name="plain-text-if-not-exists">true</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.volume.project-handbook.title</ac:parameter>
                    <ac:parameter ac:name="plain-text-if-not-exists">true</ac:parameter>
                  </ac:structured-macro>
                </li>
              </ul>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="projectdoc-content-marker">
        <ac:parameter ac:name="id">planning-core</ac:parameter>
        <ac:parameter ac:name="css">projectdoc-homepage-panel-core</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-aside-panel-macro">
            <ac:parameter ac:name="css">projectdoc-pagetype-spacehome, projectdoc-addon-agile-planning, projectdoc-panel-agile-planning, projectdoc-nav-panel</ac:parameter>
            <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.content.planning"/></ac:parameter>
            <ac:rich-text-body>
              <ul>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.iteration.home.title</ac:parameter>
                  </ac:structured-macro>
                  <ul>
                    <li>
                      <ac:structured-macro ac:name="projectdoc-link-wiki">
                        <ac:parameter ac:name="page">projectdoc.content.retrospective.index.all.title</ac:parameter>
                        <ac:parameter ac:name="label"><at:i18n at:key="projectdoc.doctype.retrospective.home"/></ac:parameter>
                        <ac:parameter ac:name="plain-text-if-not-exists">true</ac:parameter>
                      </ac:structured-macro>
                    </li>
                    <li>
                      <ac:structured-macro ac:name="projectdoc-link-wiki">
                        <ac:parameter ac:name="page">projectdoc.content.review.index.all.title</ac:parameter>
                        <ac:parameter ac:name="label"><at:i18n at:key="projectdoc.doctype.review.home"/></ac:parameter>
                        <ac:parameter ac:name="plain-text-if-not-exists">true</ac:parameter>
                      </ac:structured-macro>
                    </li>
                  </ul>
                </li>
              </ul>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="projectdoc-content-marker">
        <ac:parameter ac:name="id">backlogs-core</ac:parameter>
        <ac:parameter ac:name="css">projectdoc-homepage-panel-core</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-aside-panel-macro">
            <ac:parameter ac:name="css">projectdoc-pagetype-spacehome, projectdoc-addon-agile-planning, projectdoc-panel-agile-planning, projectdoc-nav-panel</ac:parameter>
            <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.content.planning.backlogs"/></ac:parameter>
            <ac:rich-text-body>
              <ul>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.improvement.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.product-backlog.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
                <li>
                  <ac:structured-macro ac:name="projectdoc-link-wiki">
                    <ac:parameter ac:name="page">projectdoc.content.user-story.home.title</ac:parameter>
                  </ac:structured-macro>
                </li>
              </ul>
            </ac:rich-text-body>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>
      <ac:structured-macro ac:name="projectdoc-aside-panel-macro">
        <ac:parameter ac:name="css">projectdoc-pagetype-spacehome, projectdoc-addon-agile-planning, projectdoc-panel-recently-updated</ac:parameter>
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.home.label.recently-updated"/></ac:parameter>
        <ac:rich-text-body>
          <p>
            <ac:structured-macro ac:name="recently-updated">
              <ac:parameter ac:name="max">10</ac:parameter>
              <ac:parameter ac:name="hideHeading">true</ac:parameter>
              <ac:parameter ac:name="theme">concise</ac:parameter>
              <ac:parameter ac:name="types">page, comment, blogpost, spacedesc</ac:parameter>
            </ac:structured-macro>
          </p>
        </ac:rich-text-body>
      </ac:structured-macro>
    </ac:layout-cell>
  </ac:layout-section>
</ac:layout>
