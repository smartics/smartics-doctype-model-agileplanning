/*
 * Copyright 2014-2018 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.agileplanning;

import com.atlassian.confluence.plugins.createcontent.api.events.SpaceBlueprintCreateEvent;
import  com.atlassian.confluence.plugins.createcontent.api.blueprint.SpaceBlueprint;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.event.api.EventListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Listens to space creation events.
 */
public class ProjectDocSpaceEventListener {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private static final String MODULE_KEY =
      "de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-agileplanning:projectdoc-space-blueprint-agileplanning";

  /**
   * Reference to the logger for this class.
   */
  private static final Logger LOG =
      LoggerFactory.getLogger(ProjectDocSpaceEventListener.class);

  // --- members --------------------------------------------------------------

  private final UserAccessor userAccessor;

  private final SpacePermissionManager spacePermissionManager;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public ProjectDocSpaceEventListener(final UserAccessor userAccessor,
      final SpacePermissionManager spacePermissionManager) {
    this.userAccessor = userAccessor;
    this.spacePermissionManager = spacePermissionManager;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  /**
   * Adjusts newly created spaces.
   *
   * @param event the space event to apply adjustments to.
   */
  @EventListener
  @SuppressWarnings("deprecation")
  public void onSpaceBlueprintCreate(final SpaceBlueprintCreateEvent event) {
    final SpaceBlueprint spaceBlueprint = event.getSpaceBlueprint();
    final String moduleKey = spaceBlueprint.getModuleCompleteKey();
    final Map<String, Object> context = event.getContext();
    if (!moduleKey.equals(MODULE_KEY)) {
      return;
    }

    if (LOG.isDebugEnabled()) {
      LOG.debug("Creating team page part ...");
    }
    final TeamRenderer team =
        new TeamRenderer(userAccessor, spacePermissionManager);
    team.addTeam(event, context);
    if (LOG.isDebugEnabled()) {
      LOG.debug("Created team page part.");
    }
  }

  // --- object basics --------------------------------------------------------

}
