/*
 * Copyright 2013-2025 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.agileplanning.app.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Bean;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.user.UserAccessor;

import de.smartics.projectdoc.agileplanning.subspace.PartitionContextProvider;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderSupportService;
import de.smartics.projectdoc.agileplanning.ProjectDocSpaceEventListener;

@Configuration
@Import({ConfluenceComponentImports.class, ProjectdocComponentImports.class})
public class AppServicesConfig {
  @Bean
  public PartitionContextProvider partitionContextProvider(
      final ContextProviderSupportService support) {
    return new PartitionContextProvider(support);
  }

  @Bean
  public ProjectDocSpaceEventListener projectDocSpaceEventListener(final UserAccessor userAccessor,
      final SpacePermissionManager spacePermissionManager) {
    return new ProjectDocSpaceEventListener(userAccessor, spacePermissionManager);
  }
}