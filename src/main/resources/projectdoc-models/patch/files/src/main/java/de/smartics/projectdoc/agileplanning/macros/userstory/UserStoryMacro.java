/*
 * Copyright 2014-2018 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.agileplanning.macros.userstory;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.RenderUtils;
import de.smartics.projectdoc.atlassian.confluence.api.doctypes.UserStoryDoctype;
import de.smartics.projectdoc.atlassian.confluence.document.DocumentProperty;
import de.smartics.projectdoc.atlassian.confluence.document.PropertiesDocument;
import de.smartics.projectdoc.atlassian.confluence.query.PropertiesDocumentCreator;
import de.smartics.projectdoc.atlassian.confluence.tools.macro.CoreMacroServices;
import de.smartics.projectdoc.atlassian.confluence.util.ProjectdocUser;
import de.smartics.projectdoc.atlassian.confluence.util.UnifiedApiMacro;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * Provides a user story in a representation to be beautified by CSS.
 */
public class UserStoryMacro extends UnifiedApiMacro {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * The name to the parameter that references the document. If the document is
   * not set, the current page is used.
   */
  private static final String PARAM_DOCUMENT = "document";

  /**
   * The name to the parameter that stores the intro text for the actor.
   */
  private static final String PARAM_INTRO = "intro";

  // --- members --------------------------------------------------------------

  private final CoreMacroServices core;

  /**
   * Helper to create the instance of a properties document.
   */
  private final PropertiesDocumentCreator creator;

  private final ProjectdocUser user;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public UserStoryMacro(final CoreMacroServices core) {
    this.core = core;
    this.user = core.getUser();
    this.creator = new PropertiesDocumentCreator(core);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  @Override
  public RenderMode getBodyRenderMode() {
    return RenderMode.NO_RENDER;
  }

  @Override
  public BodyType getBodyType() {
    return BodyType.NONE;
  }

  @Override
  public OutputType getOutputType() {
    return OutputType.BLOCK;
  }

  // --- business -------------------------------------------------------------

  @Override
  public String execute(final Map<String, String> parameters, final String body,
      final ConversionContext conversionContext)
      throws MacroExecutionException {
    final I18NBean i18n = user.createI18n();

    final String document = getParameter(parameters, PARAM_DOCUMENT, null);

    final PropertiesDocument propertiesDocument =
        creator.fetchDocument(conversionContext, document);
    if (propertiesDocument == null) {
      final String errorMessage = RenderUtils.blockError(
          i18n.getText("projectdoc.macros.userStory.error.noDocument",
              new String[] {StringEscapeUtils.escapeHtml4(document)}), "");
      return errorMessage;
    }

    final String content =
        createContent(conversionContext, parameters, i18n, propertiesDocument);
    return content;
  }

  private String createContent(final ConversionContext baseContext,
      final Map<String, String> parameters, final I18NBean i18n,
      final PropertiesDocument document) {
    final ConversionContext context = document.createContext(baseContext);

    final String renderedActor =
        renderProperty(i18n, document, context, UserStoryDoctype.ACTOR);
    final String renderedGoal =
        renderProperty(i18n, document, context, UserStoryDoctype.GOAL);
    final String renderedBusinessValue = renderProperty(i18n, document, context,
        UserStoryDoctype.BUSINESS_VALUE);

    final String asA = parameters.get(PARAM_INTRO);
    final StringBuilder buffer = new StringBuilder(256);
    buffer.append("<div class=\"user-story-workitem\"><span class=\"actor\">")
        .append(surroundWithSpan(asA, "first-word")).append(' ')
        .append(surroundWithSpan(renderedActor, "actor-element"))
        .append("</span> <span class=\"goal\">")
        .append(surroundFirstWordWithSpan(renderedGoal))
        .append("</span> <span class=\"business-value\">")
        .append(surroundFirstWordWithSpan(renderedBusinessValue))
        .append("</span></div>");
    final String content = buffer.toString();
    return content;
  }

  private String surroundWithSpan(final String element, final String cssClass) {
    return "<span class=\"" + cssClass + "\">" + element + "</span> ";
  }

  private String surroundFirstWordWithSpan(final String element) {
    final String surrounded;
    if (StringUtils.isNotBlank(element)) {
      final String arr[] = element.split(" ", 2);
      if (arr.length >= 2) {
        final String firstWord = arr[0];
        final String theRest = arr[1];
        final StringBuilder builder = new StringBuilder(64);
        builder.append("<span class=\"first-word\">").append(firstWord)
            .append("</span> ").append(surroundWithSpan(theRest, "rest"));
        surrounded = builder.toString();
      } else {
        surrounded = element;
      }
    } else {
      surrounded = element;
    }
    return surrounded;
  }

  private String renderProperty(final I18NBean i18n,
      final PropertiesDocument document, final ConversionContext context,
      final String propertyName) {
    final String name = i18n.getText(propertyName);
    final DocumentProperty property = document.getProperty(name);
    if (property != null) {
      final String value = property.renderAndSet(context, creator);
      if (StringUtils.isNotBlank(value)) {
        return value;
      }
    }
    return "n/a";
  }

  // --- object basics --------------------------------------------------------

}
