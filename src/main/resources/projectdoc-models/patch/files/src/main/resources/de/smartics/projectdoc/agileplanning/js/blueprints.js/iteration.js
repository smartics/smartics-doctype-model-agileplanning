require(['ajs', 'confluence/root', 'de/smartics/projectdoc/modules/core'], function (AJS, Confluence, PROJECTDOC) {
  "use strict";

  AJS.bind("blueprint.wizard-register.ready", function () {
    Confluence.Blueprint.setWizard('de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-agileplanning:create-doctype-template-iteration',
      function (wizard) {
        wizard.on('pre-render.page1Id', function (e, state) {
          state.wizardData.selectedWebItemData = PROJECTDOC.prerenderWizard(e, state);
        });

        wizard.on("post-render.page1Id", function (ev, state) {
          AJS.$('#projectdoc-duration-from').datepicker({
            dateFormat: "yy-mm-dd"
          });
          AJS.$('#projectdoc-duration-to').datepicker({
            dateFormat: "yy-mm-dd"
          });

          const title = state.wizardData.title;
          if (title) {
            AJS.$('#projectdoc_doctype_common_name').val(title);
            AJS.$('#projectdoc-duration-from').focus();
          }

          wizard.assembleSubmissionObject = PROJECTDOC.assemblePageCreationData;
        });

        wizard.on('submit.page1Id', function (e, state) {
          const name = state.pageData["projectdoc_doctype_common_name"];
          if (!name) {
            alert('Please provide a name for the iteration.');
            return false;
          }

          const specifiedStartDate = state.pageData["projectdoc-duration-from"];
          if (!specifiedStartDate) {
            alert('Please provide a start date for the iteration.');
            return false;
          }

          const specifiedEndDate = state.pageData["projectdoc-duration-to"];
          if (!specifiedEndDate) {
            alert('Please provide an end date for the iteration.');
            return false;
          }

          const shortDescription = state.pageData["projectdoc_doctype_common_shortDescription"];
          if (!shortDescription) {
            alert('Please provide a short description for the iteration.');
            return false;
          }

          PROJECTDOC.adjustToLocation(state);
        });
      });
  });
});