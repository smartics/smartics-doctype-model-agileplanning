# projectdoc Blueprints for Agile Planning

## Overview

This project specifies a model to create a free [doctype add-on](https://www.smartics.eu/confluence/x/nQHJAw)
for the [projectdoc Toolbox](https://www.smartics.eu/confluence/x/1YEp)
for [Confluence](https://www.atlassian.com/software/confluence).

The add-on provides the blueprints to create pages for

  * Iterations, Retrospectives, Reviews
  * User Stories
  * Announcements
  * Product Backlogs, Improvement Backlogs

It also provides a space blueprint to get started with your software documentation project quickly.

## Fork me!
Feel free to fork this project to adjust the templates according to your project requirements.

The projectdoc Core Doctypes Add-on is licensed under [Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

## Documentation

For more information please visit

  * the [add-on's homepage](https://www.smartics.eu/confluence/display/PDAC1/Doctypes+for+Agile+Planning)
  * the [add-on on the Atlassian Marketplace](https://marketplace.atlassian.com/plugins/de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-agileplanning)
  * [projectdoc Toolbox Online Manual](https://www.smartics.eu/confluence/x/EAFk)
  * [Doctype Maven Plugin](https://www.smartics.eu/confluence/x/zgDqAw)
